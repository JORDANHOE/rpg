﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System;
using System.IO;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ContinueGame : MonoBehaviour {

	private PlayerData data;
	private Button continueButton;

	void Start () {

		data = GameObject.Find ("Consistencies").GetComponent <PlayerData> ();
		continueButton = gameObject.GetComponent <Button> ();
	}

	void Update () {

		if (data.created) {

			continueButton.interactable = true;
		} else {

			continueButton.interactable = false;
		}
	}

	public void LoadGame () {

		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {

			data.Load ();
			SceneManager.LoadScene ("World", LoadSceneMode.Single);
		}
	}
}
