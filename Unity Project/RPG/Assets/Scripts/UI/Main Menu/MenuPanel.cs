﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuPanel : MonoBehaviour {

	public int menuID;

	private GameObject obj;

	void Start () {

		obj = this.gameObject;
	}

	void Update () {

		if (MenuHandler.currentPanel == menuID) {

			for (int i = 0; i < obj.transform.childCount; i++) {

				obj.transform.GetChild (i).gameObject.SetActive (true);
			}
		} else {

			for (int i = 0; i < obj.transform.childCount; i++) {

				obj.transform.GetChild (i).gameObject.SetActive (false);
			}
		}
	}
}
