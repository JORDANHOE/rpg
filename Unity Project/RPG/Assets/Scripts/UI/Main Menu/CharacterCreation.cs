﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CharacterCreation : MonoBehaviour {

	private Text hairStyleName;
	private Button createCharacter;
	private InputField nameInput;
	private PlayerData pd;
	private AnimationBank ab;

	void Start () {

		pd = GameObject.Find ("Consistencies").GetComponent <PlayerData> ();
		ab = GameObject.Find ("Consistencies").GetComponent <AnimationBank> ();

		createCharacter = GameObject.Find ("Create Button").GetComponent <Button> ();
		nameInput = GameObject.Find ("Name Input").GetComponent <InputField> ();

		hairStyleName = GameObject.Find ("Hairstyle Name").GetComponent <Text>();

		pd.skinToneR = 0.984f;
		pd.skinToneG = 0.898f;
		pd.skinToneB = 0.847f;
		pd.hairColorR = 0.403f;
		pd.hairColorG = 0.341f;
		pd.hairColorR = 0.301f;
	}

	void Update () {

		hairStyleName.text = ab.hairstyleIdleDatabase [pd.hairStyle].accessoryName;

		if (nameInput.text.Length > 0) {
			
			pd.characterName = nameInput.text;
			createCharacter.interactable = true;
		} else {

			createCharacter.interactable = false;
		}
	}

	public void ChangeSkinTone (GameObject go) {

		pd.skinToneR = go.GetComponent <Image> ().color.r;
		pd.skinToneG = go.GetComponent <Image> ().color.g;
		pd.skinToneB = go.GetComponent <Image> ().color.b;

	}

	public void ChangeHairColor (GameObject go) {

		pd.hairColorR = go.GetComponent <Image> ().color.r;
		pd.hairColorG = go.GetComponent <Image> ().color.g;
		pd.hairColorB = go.GetComponent <Image> ().color.b;
	}

	public void ChangeHairStyle (int value) {

		int current = pd.hairStyle;

		if (value == 0) {

			if (current == 0) {

				pd.hairStyle = ab.hairstyleIdleDatabase.Count - 1;
			} else {

				pd.hairStyle--;
			}
		} else if (value == 1) {

			if (current == ab.hairstyleIdleDatabase.Count - 1) {

				pd.hairStyle = 0;
			} else {

				pd.hairStyle++;
			}
		}
	}

	public void CreateCharacter () {

		pd.equipmentHead = 0;
		pd.equipmentFeet = 0;
		pd.equipmentTorso = 0;
		pd.equipmentLegs = 0;
		pd.created = true;

		pd.Save ();
		SceneManager.LoadScene ("World", LoadSceneMode.Single);
	}
}
