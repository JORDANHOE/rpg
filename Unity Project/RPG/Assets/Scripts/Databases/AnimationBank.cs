﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationState {

	public string featureName;
	public List <Sprite> frames;
}

[System.Serializable]
public class Accessory {

	public string accessoryName;
	public List <AnimationState> animations;
}

public class AnimationBank : MonoBehaviour {

	public List<AnimationState> maleIdleDatabase = new List<AnimationState> ();
	public List<AnimationState> maleWalkDatabase = new List<AnimationState> ();

	public List<Accessory> hairstyleIdleDatabase = new List<Accessory> ();
	public List<Accessory> hairstyleWalkDatabase = new List<Accessory> ();
}