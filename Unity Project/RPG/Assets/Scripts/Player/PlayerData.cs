﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
class SaveFile {

	public int hair;
	public int equipHead;
	public int equipTorso;
	public int equipFeet;
	public int equipLegs;
	public int gender;
	public string name;
	public float skinR;
	public float skinG;
	public float skinB;
	public float hairR;
	public float hairG;
	public float hairB;

	public bool saveCreated;
}


public class PlayerData : MonoBehaviour {

	public int hairStyle;

	public int characterGender;

	public string characterName;

	public float skinToneR;
	public float skinToneG;
	public float skinToneB;

	public float hairColorR;
	public float hairColorG;
	public float hairColorB;

	public int equipmentHead;
	public int equipmentTorso;
	public int equipmentLegs;
	public int equipmentFeet;

	public bool created;

	void Start () {

		DontDestroyOnLoad (gameObject);

		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {

			Load ();
		}
	}
		
	public void Save () {

		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");

		SaveFile data = new SaveFile ();

		data.name = characterName;
		data.gender = characterGender;

		data.skinR = skinToneR;
		data.skinG = skinToneG;
		data.skinB = skinToneB;

		data.hairR = hairColorR;
		data.hairG = hairColorG;
		data.hairB = hairColorB;

		data.hair = hairStyle;
		data.equipHead = equipmentHead;
		data.equipTorso = equipmentTorso;
		data.equipLegs = equipmentLegs;
		data.equipFeet = equipmentFeet;

		data.saveCreated = created;

		bf.Serialize (file, data);
		file.Close ();
	}

	public void Load () {

		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {

			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
			SaveFile data = (SaveFile) bf.Deserialize (file);
			file.Close ();

			characterName = data.name;
			characterGender = data.gender;

			skinToneR = data.skinR;
			skinToneG = data.skinG;
			skinToneB = data.skinB;

			hairColorR = data.hairR;
			hairColorG = data.hairG;
			hairColorB = data.hairB;

			hairStyle = data.hair;
			equipmentHead = data.equipHead;
			equipmentTorso = data.equipTorso;
			equipmentLegs = data.equipLegs;
			equipmentFeet = data.equipFeet;
			created = data.saveCreated;
		}
	}
}
