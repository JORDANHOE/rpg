﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class FrameAnimation {

	public string animationName;
	public List<Sprite> frames;
}

public class PlayerDrawHandler : MonoBehaviour {

	public bool preview = false;
	public float animationSpeed = 5f;

	private AnimationBank bank;
	private PlayerData data;

	private float timer = 0f;
	[HideInInspector]
	public int playerDirection = 0;			//0 = South, 1 = North, 2 = West, 3 = East
	[HideInInspector]
	public int currentFrame = 0;
	[HideInInspector]
	public int movementState = 0;			//0 = Idle, 1 = Walking

	private SpriteRenderer playerSprite;
	private SpriteRenderer hairSprite;

	private Image playerInterfacePreview;
	private Image hairInterfacePreview;

	void Start () {

		bank = GameObject.Find ("Consistencies").GetComponent <AnimationBank> ();
		data = GameObject.Find ("Consistencies").GetComponent <PlayerData> ();

		if (!preview) {
			
			playerSprite = gameObject.GetComponent <SpriteRenderer> ();
			hairSprite = transform.GetChild (0).GetComponent <SpriteRenderer> ();
		} else {
			
			playerInterfacePreview = gameObject.GetComponent <Image> ();
			hairInterfacePreview = transform.GetChild (0).GetComponent <Image> ();
		}
	}

	void Update () {

		Timer ();

		if (!preview) {

			playerSprite.color = new Color (data.skinToneR, data.skinToneG, data.skinToneB, 1);
			hairSprite.color = new Color (data.hairColorR, data.hairColorG, data.hairColorB, 1);
			
			if (data.characterGender == 0) {
				
				if (movementState == 0) {
	
					playerSprite.sprite = bank.maleIdleDatabase [playerDirection].frames [currentFrame];
					hairSprite.sprite = bank.hairstyleIdleDatabase [data.hairStyle].animations [playerDirection].frames [currentFrame];
				} else if (movementState == 1) {

					playerSprite.sprite = bank.maleWalkDatabase [playerDirection].frames [currentFrame];
					hairSprite.sprite = bank.hairstyleWalkDatabase [data.hairStyle].animations [playerDirection].frames [currentFrame];
				}
			}
		} else {

			playerInterfacePreview.color = new Color (data.skinToneR, data.skinToneG, data.skinToneB, 1);
			hairInterfacePreview.color = new Color (data.hairColorR, data.hairColorG, data.hairColorB, 1);

			if (data.characterGender == 0) {

				playerInterfacePreview.sprite = bank.maleWalkDatabase [0].frames [currentFrame];
				hairInterfacePreview.sprite = bank.hairstyleWalkDatabase [data.hairStyle].animations [0].frames [currentFrame];
			}
		}
	}

	private void Timer () {

		timer += (animationSpeed * Time.deltaTime);

		if (timer >= 1f) {

			if (currentFrame < 3) {

				currentFrame++;
				timer = 0f;
			} else {

				currentFrame = 0;
				timer = 0f;
			}
		}
	}
}
