﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementHandler : MonoBehaviour {

	public float speed = 5f;
	public float acceleration = 1f;

	private PlayerDrawHandler pdh;
	private Rigidbody2D rb2d;

	void Start () {

		rb2d = this.GetComponent <Rigidbody2D> ();
		pdh = this.GetComponent <PlayerDrawHandler> ();
	}
	

	void FixedUpdate () {

		float x = Input.GetAxis ("Horizontal");
		float y = Input.GetAxis ("Vertical");

		Vector2 moveDirection = new Vector2 (Mathf.Lerp (0f, x * speed, acceleration), Mathf.Lerp (0f, y * speed, acceleration));

		if (moveDirection == new Vector2 (0, 0)) {

			pdh.movementState = 0;
			pdh.currentFrame = 0;
		}
		if (moveDirection.y < 0) {


			pdh.playerDirection = 0;
			pdh.movementState = 1;
		} else if (moveDirection.y > 0) {

			pdh.playerDirection = 1;
			pdh.movementState = 1;
		} else if (moveDirection.x < 0) {

			pdh.playerDirection = 2;
			pdh.movementState = 1;
		} else if (moveDirection.x > 0) {

			pdh.playerDirection = 3;
			pdh.movementState = 1;
		}

		rb2d.velocity = moveDirection; 	


	}
}
